#include <iostream>
#include <cstring>
#include <unistd.h>
#include <thread>

#include "gtrace.h"

static std::string getUserName() {
	char* id = getlogin();
	return std::string(id);
}

int main(int argc, char* argv[]) {
	char wd[BUFSIZ];
	memset(wd, 0, BUFSIZ);
	getcwd(wd, BUFSIZ);
	GTRACE("demon started login=%s argv[0]=%s getcwd=%s", getUserName().data(), argv[0], wd);
	int count = 5;
	if (argc >= 2)
		count = std::stoi(argv[1]);
	for (int i = 0; i < count; i++) {
		printf("%d\n", i);
		GTRACE("%d", i);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	GTRACE("demon terminated");
}

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QProcess>
#include <QThread>

struct MyThread : QThread {
	volatile bool active_{true};
	QProcess* process_;
	void run();
};

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
	Q_OBJECT

public:
	Widget(QWidget *parent = nullptr);
	~Widget();

	QProcess process_;
	MyThread* myThread_{nullptr};

private slots:
	void on_pbStart_clicked();

	void on_pbStop_clicked();

private:
	Ui::Widget *ui;
};

#endif // WIDGET_H

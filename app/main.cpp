#include "widget.h"

#include <QApplication>
#include <QDebug>
#include <QFile>

bool copyFileFromAssets(QString fileName, QFile::Permissions permissions) {
	QString sourceFileName = QString("assets:/") + fileName;
	QFile sFile(sourceFileName);
	QFile dFile(fileName);
	if (!dFile.exists()) {
		if (!sFile.exists()) {
			qWarning() << QString("src file(%1) not exists").arg(sourceFileName);
			return false;
		}

		if (!sFile.copy(fileName)) {
			qWarning() << QString("file copy(%1) return false").arg(fileName);
			return false;
		}
		QFile::setPermissions(fileName, permissions);
	}
	return true;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	copyFileFromAssets("demon", QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner);

	Widget w;
	w.show();
	return a.exec();
}

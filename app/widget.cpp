#include "widget.h"
#include "ui_widget.h"

#include <QDebug>

void MyThread::run() {
	while (active_) {
		QProcess::ProcessState state = process_->state();
		if (state != QProcess::Running) {
			qDebug() << state;
			break;;
		}

		bool res = process_->waitForReadyRead(500);
		// qDebug() << "waitForReadyRead return " << res;
		if (!res)
			continue;

		QByteArray ba = process_->readAll();
		qDebug() << ba;
	}
	process_->moveToThread(QApplication::instance()->thread());
}

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);

	QFile file("command.txt");
	QStringList stringList;
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))	{
		while(!file.atEnd()) {
			QString s = file.readLine();
			if (s.at(s.length() - 1) == '\n')
				s = s.mid(0, s.length() - 1);
			stringList.append(s);
		}
		file.close();
		ui->pteCommand->setPlainText(stringList.join('\n'));
	}
}

Widget::~Widget()
{
	QFile file("command.txt");
	QStringList stringList = ui->pteCommand->toPlainText().split('\n');
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))	{
		for (QString s: stringList) {
			s += "\n";
			QByteArray ba = qPrintable(s);
			file.write(ba);
		}
		file.close();
	}
	delete ui;
}

void Widget::on_pbStart_clicked()
{
	QString command = ui->pteCommand->toPlainText();
	QStringList arguments = command.split("\n");
	qDebug() << arguments;
	if (arguments.count() == 0) return;

	QString program = arguments.at(0);
	arguments.removeAt(0);

	process_.start(program, arguments);
	bool res = process_.waitForStarted();
	if (!res) {
		qDebug() << "waitForStarted return false";
		return;
	}

	myThread_ = new MyThread;

	qDebug() << "process thread is" << process_.thread();
	qDebug() << "main thread is" << QApplication::instance()->thread();
	qDebug() << "my thread is" << myThread_;

	myThread_->process_ = &process_;
	process_.moveToThread(myThread_);
	myThread_->start();
}

void Widget::on_pbStop_clicked()
{
	if (myThread_ != nullptr) {
		myThread_->active_ = false;
		myThread_->wait();
		delete myThread_;
	}

	process_.terminate();
	process_.kill();

}
